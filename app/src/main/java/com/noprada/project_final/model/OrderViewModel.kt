package com.noprada.project_final.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class OrderViewModel : ViewModel() {
    private val _namemenu = MutableLiveData<String>("")
    val name_menu: LiveData<String> = _namemenu

    private val _level = MutableLiveData<String>("")
    val level: LiveData<String> = _level

    private val _total = MutableLiveData<String>("")
    val total: LiveData<String> = _total


    fun hasNoName_menuSet(): Boolean {
        return _namemenu.value.isNullOrEmpty()
    }
    fun hasNolevelSet(): Boolean {
        return _level.value.isNullOrEmpty()
    }
    fun hasNototalSet(): Boolean {
        return _total.value.isNullOrEmpty()
    }
    // เก็บข้อมูล
    fun setName_menu(namecha: String) {
        _namemenu.value = namecha
    }

    fun setLevel(level_sugar: String) {
        _level.value = level_sugar
    }

    fun settotal(price_total: String) {
        _total.value = price_total
    }
    //reset ข้อมูล
    fun resetOrder() {
        _namemenu.value = ""
        _level.value = ""
        _total.value = ""
    }
}

