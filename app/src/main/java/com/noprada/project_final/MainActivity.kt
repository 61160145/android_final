package com.noprada.project_final

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController //ใส่ lateinit เพื่อไปถูกเชท ที่ setupActionBar



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //get navHost
        //nav_host_fragment ใส่ตัว Fragment ทั้งหมดอยู่
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        //get navController

        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)// setup navController ขึ้นบาร์ของแต่ละ Fragment and back

    }

    override fun onSupportNavigateUp(): Boolean { //สามารถกด < กลับได้
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}