package com.noprada.project_final

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.noprada.project_final.databinding.FragmentMenuBinding
import com.noprada.project_final.model.OrderViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [Menu_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Menu_Fragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var binding: FragmentMenuBinding? = null
    private val sharedViewModel: OrderViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentMenuBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply{
           viewModel = sharedViewModel
            menuFragment = this@Menu_Fragment
        }

    }

    fun goToNextScreen() {
        if(sharedViewModel.hasNolevelSet()) {
            sharedViewModel.setLevel(getString(R.string.zero))
        }
        findNavController().navigate(R.id.action_menu_Fragment_to_level_Fragment)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

}