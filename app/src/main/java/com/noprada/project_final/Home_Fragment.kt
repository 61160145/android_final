package com.noprada.project_final

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.noprada.project_final.databinding.FragmentHomeBinding
import com.noprada.project_final.model.OrderViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Home_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Home_Fragment : Fragment() {
    // TODO: Rename and change types of parameters


    private var binding: FragmentHomeBinding? = null   //การใส่ ? เพื่อที่fragment ยอมให้้ binding เป็น null ได้

    private val sharedViewModel: OrderViewModel by activityViewModels()// sharedViewModel ทำหน้าที่แทน activityViewModel(เก็บFragment ทุกตัวไว้เพื่อเรียกใช้งาน)


    override fun onCreateView( //เชื่อม layout ได้ตัว binding
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentHomeBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }
    //เอาตัว component ต่างๆมาเชื่อมกัน จัดการตัวแปรเช่น ปุ่ม ช่องข้อความ
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply { //apply ในสโคป เหมือนอยู่ใน binding
            viewModel = sharedViewModel
            homeFragment = this@Home_Fragment  //การเชื่อมหน้า Fragment นั้นๆ สามารถย้ายปุ่มไปใส่ใน Fragment นั้นๆได้
        }
    }

    fun orderName() {
        if(sharedViewModel.hasNoName_menuSet()) {
            sharedViewModel.setName_menu(getString(R.string.coffee))
        } //ตัวเช็คถ้าไม่มีตัวเช็คไว้ก็จะเป็นตัวกาแฟ
        findNavController().navigate(R.id.action_home_Fragment_to_menu_Fragment)// เชื่อมไปอีกหน้า

    }


    override fun onDestroyView() { //ทำลาย
        binding = null
        super.onDestroyView()
    }

}