package com.noprada.project_final

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.noprada.project_final.databinding.FragmentSummaryBinding
import com.noprada.project_final.model.OrderViewModel

// TODO: Rename parameter arguments, choose names that match


/**
 * A simple [Fragment] subclass.
 * Use the [Summary_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Summary_Fragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var binding: FragmentSummaryBinding? = null
    private val sharedViewModel: OrderViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       val fragmentBinding = FragmentSummaryBinding.inflate(inflater,container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel = sharedViewModel
            summaryFragment = this@Summary_Fragment
        }

    }
    fun goToNextScreen() {
        findNavController().navigate(R.id.action_summary_Fragment_to_complete_Fragment)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    fun reset() {
        sharedViewModel.resetOrder()
        findNavController().navigate(R.id.action_summary_Fragment_to_menu_Fragment)
    }
}